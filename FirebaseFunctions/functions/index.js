const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

exports.sendNotification = functions.https.onCall((data, context) => {
    const payload = {
        notification: {
            title: data.title,
            body: data.message,
        }
    }
    console.log(payload.notification)
    admin.messaging().sendToDevice(data.to, payload)
});