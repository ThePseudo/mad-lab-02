package com.mad.market.ui.items

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.toObject
import com.mad.market.data.Item
import com.mad.market.*
import java.io.File
import java.io.FileInputStream
import java.lang.NullPointerException

class ItemEditViewModel(private val itemID: String) : ViewModel() {
    var localPicture: MutableLiveData<String?> = MutableLiveData(null)
    var item: MutableLiveData<Item> = MutableLiveData(Item(id = itemID, authorID = getCurrentUserID().orEmpty()))
    var newItem: MutableLiveData<Boolean> = MutableLiveData(true)
    var hasLocalPicture = false
    private var alreadyDownloaded = false


    init {
        database.collection("items").document(itemID).addSnapshotListener { snapshot, error ->
            if(error != null)
            {
                return@addSnapshotListener
            }
            if(snapshot != null && snapshot.exists()) {
                val i = snapshot.toObject<Item>()
                if(i != null && i != Item()) {
                    newItem.value = false
                    // Then update item values
                    item.value = i
                    Log.e("Item edit", "old item")
                }
            }
        }
    }

    fun getImage(forceDownload: Boolean = false, thenRun: (image: String?) -> Unit = {}) {
        if(!hasLocalPicture) {
            if(item.value!!.image != "") {
                alreadyDownloaded = true
                downloadImageFromFirebase("items/${item.value!!.image}", item.value!!.image, forceDownload) {
                    try { //if we switch screen too fast
                        localPicture.value = it.orEmpty()
                        thenRun(it)
                    } catch (e: NullPointerException) {}
                }
            }
        }
        else {
            thenRun(localPicture.value)
        }
    }

    fun save(thenRun: () -> Unit = {}) {
        val firebaseImagePath = storage.child("items")
            .child("$itemID.jpg")
        if(localPicture.value != null) {
            val fis = FileInputStream(File(localPicture.value))
            firebaseImagePath.putStream(fis).addOnSuccessListener {
                item.value!!.image = "$itemID.jpg"
                database.collection("items").document(itemID).set(item.value!!).addOnSuccessListener {
                    getImage(true) { // refresh cached image
                        thenRun()
                    }
                }
            }
        }
        else {
            database.collection("items").document(itemID).set(item.value!!).addOnSuccessListener {
                thenRun()
            }
        }
    }
}
