package com.mad.market.ui.profile

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Paint
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.format.DateFormat
import android.text.format.DateFormat.getDateFormat
import android.view.*
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.PopupMenu
import android.widget.TextView.BufferType.EDITABLE
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.mad.market.*
import com.mad.market.ui.maps.MapsFragment
import com.mad.market.ui.maps.MapsFragment.Companion.CONTEXT_EDIT_USER
import com.mad.market.ui.maps.MapsFragment.Companion.CONTEXT_ID
import com.mad.market.ui.maps.MapsFragment.Companion.RESULT_LANG_SHOULD_UPDATE
import com.mad.market.ui.maps.MapsFragment.Companion.RESULT_LATLNG_ID
import kotlinx.android.synthetic.main.fragment_edit_profile.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class EditProfileFragment : Fragment() {
    val userID: String = getCurrentUserID().orEmpty()
    val userEmail: String = FirebaseAuth.getInstance().currentUser!!.email.orEmpty()

    val width: Int = 500
    val height: Int = 500

    private lateinit var editViewModel: EditProfileViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_edit_profile, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        editViewModel = ViewModelProvider(this, object: ViewModelProvider.NewInstanceFactory() {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return EditProfileViewModel(userID, userEmail) as T
            }
        }).get(EditProfileViewModel::class.java)

        // Edit VM
        editViewModel.run {
            // PASS DATA BACK
            val activity = (requireActivity() as MainActivity)
            val shouldUpdate = activity.getDataFromExternalFragment(RESULT_LANG_SHOULD_UPDATE) as Boolean?
            if(shouldUpdate != null && shouldUpdate) {
                activity.passDataToExternalFragment(bundleOf(RESULT_LANG_SHOULD_UPDATE to false))
                val latLng: LatLng = activity.getDataFromExternalFragment(RESULT_LATLNG_ID) as LatLng
                user.value!!.latitude = latLng.latitude
                user.value!!.longitude = latLng.longitude
            }
            if(user.value!!.latitude == null && user.value!!.longitude == null) {
                //map.requireView().visibility = View.GONE
            }
            else {
                //map.requireView().visibility = View.VISIBLE
            }
            localPicture.observe(viewLifecycleOwner, Observer {
                if(it == null) {
                    rotate_left_button.visibility = View.GONE
                    rotate_right_button.visibility = View.GONE
                    editViewModel.user.value!!.hasPropic = false
                }
                else {
                    rotate_left_button.visibility = View.VISIBLE
                    rotate_right_button.visibility = View.VISIBLE
                    editViewModel.user.value!!.hasPropic = true
                }
            })

            //ROTATION
            rotate_left_button.setOnClickListener{
                user.value!!.propicAngle += 270f
                user_propic.setImageBitmap(rotateImage(getRotatedBitmap(localPicture.value!!), user.value!!.propicAngle))
            }
            rotate_right_button.setOnClickListener{
                user.value!!.propicAngle += 90f
                user_propic.setImageBitmap(rotateImage(getRotatedBitmap(localPicture.value!!), user.value!!.propicAngle))
            }

            // BIRTHDAY CALENDAR
            user_birthday_te.setOnClickListener {
                val date: Date = try {
                    Date(user.value!!.birthday!!)
                } catch (exception: Exception) {
                    Date()
                }
                val c = Calendar.getInstance()
                c.time = date
                MaterialDatePicker.Builder.datePicker().run {
                    setTitleText(R.string.birthday)
                    setSelection(c.timeInMillis)
                    CalendarConstraints.Builder().also {
                        it.setValidator(PastDateValidator())
                        it.setEnd(Date().time)
                        this.setCalendarConstraints(it.build())
                    }
                    build().also {
                        it.addOnPositiveButtonClickListener {mTimeStamp ->
                            Calendar.getInstance().also {
                                it.timeInMillis = mTimeStamp
                                DateFormat.getDateFormat(requireContext()).run {
                                    user.value!!.birthday = it.time.time
                                    user_birthday_te.setText(getDateFormat(requireContext()).format(it.time), EDITABLE)
                                }
                            }
                        }
                        it.show(childFragmentManager, toString())
                    }
                }
            }

            user.observe(viewLifecycleOwner, Observer {
                if(it != null) {
                    if(editViewModel.user.value!!.hasPropic) {
                        editViewModel.getImage {
                            user_propic.setImageBitmap(rotateImage(getRotatedBitmap(editViewModel.localPicture.value!!), editViewModel.user.value!!.propicAngle))
                        }
                    }
                    user_fullname_te.setText(it.fullName, EDITABLE)
                    user_nickname_te.setText(it.nickName, EDITABLE)
                    user_email_te.setText(it.email, EDITABLE)
                    val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
                    mapFragment?.getMapAsync { map ->
                        map.clear()
                        map.uiSettings.isScrollGesturesEnabled = false
                        map.setOnMapClickListener {ll ->
                            val bundle = bundleOf(CONTEXT_ID to MapsFragment.CONTEXT_EDIT_USER).apply {
                                if(it.latitude != null && it.longitude != null) {
                                    putDouble(MapsFragment.CURRENT_LOCATION_LATUTUDE, it.latitude!!)
                                    putDouble(MapsFragment.CURRENT_LOCATION_LONGITUDE, it.longitude!!)
                                }
                            }
                            findNavController().navigate(R.id.action_editProfileFragment_to_mapsFragment, bundle)
                        }
                        if(it.latitude != null && it.longitude != null) {
                            map.addMarker(MarkerOptions().position(LatLng(it.latitude!!, it.longitude!!)).title(getString(R.string.current_position)))
                            map.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(it.latitude!!, it.longitude!!), DEFAULT_ZOOM.toFloat()))
                        }
                    }

                    try {
                        user_birthday_te.setText(getDateFormat(requireContext()).format(it.birthday), EDITABLE)
                    } catch (e: Exception) {}
                    user_phone_te.setText(it.phoneNumber, EDITABLE)
                    user_type_te.setText(it.userType)
                }
            })
        }
    }

    // On view created, setup listeners
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val initialPaintFlags = user_fullname_te.paintFlags
        user_fullname_te.addTextChangedListener {
            if (user_fullname_te.text.isEmpty()) {
                user_fullname_te.paintFlags =
                    user_fullname_te.paintFlags.or(Paint.UNDERLINE_TEXT_FLAG)
            } else {
                user_fullname_te.paintFlags = initialPaintFlags
            }
            editViewModel.user.value!!.fullName = user_fullname_te.text.toString()
        }

        user_nickname_te.addTextChangedListener {
            editViewModel.user.value!!.nickName = user_nickname_te.text.toString()
        }

        user_email_te.addTextChangedListener {
            editViewModel.user.value!!.email = user_email_te.text.toString()
        }

        user_phone_te.addTextChangedListener {
            editViewModel.user.value!!.phoneNumber = user_phone_te.text.toString()
        }

        user_type_te.addTextChangedListener {
            editViewModel.user.value!!.userType = user_type_te.text.toString()
        }


        //POP-UP MENU
        camera_button.setOnClickListener {
            PopupMenu(context, camera_button).also {
                it.menuInflater.inflate(R.menu.menu_context_camera, it.menu)
                it.setOnMenuItemClickListener { item ->
                    when (item.itemId) {
                        R.id.popup_gallery -> {
                            selectPicture()
                        }
                        R.id.popup_capture_photo -> {
                            takePicture()
                        }
                    }
                    true
                }
                it.show()
            }
        }

        user_type_te.setOnFocusChangeListener { _, hasFocus ->
            try {
                if(hasFocus)
                {
                    val array = resources.getStringArray(R.array.user_types)
                    val adapter = ArrayAdapter(requireContext(), R.layout.dropdown_item, array)
                    user_type_te.setAdapter(adapter)
                    user_type_te.showDropDown()
                }
            } catch (e: Exception) {}
            hideKeyboardFrom(requireContext(), requireView())
        }

        user_location_te.setOnClickListener {
            val bundle = bundleOf(CONTEXT_ID to CONTEXT_EDIT_USER)
            findNavController().navigate(R.id.action_editProfileFragment_to_mapsFragment, bundle)
        }
    }

    // Options menu
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        inflater.inflate(R.menu.menu_save, menu)
    }

    // Save
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.menu_save -> {
            when("") {
                editViewModel.user.value!!.fullName -> {
                    Snackbar.make(requireActivity().findViewById(android.R.id.content), R.string.empty_fullname, Snackbar.LENGTH_SHORT).show()
                    user_fullname_te.requestFocus()
                }
                editViewModel.user.value!!.nickName -> {
                    Snackbar.make(requireActivity().findViewById(android.R.id.content), R.string.empty_nickname, Snackbar.LENGTH_SHORT).show()
                    user_nickname_te.requestFocus()
                }
                editViewModel.user.value!!.email -> {
                    Snackbar.make(requireActivity().findViewById(android.R.id.content), R.string.empty_email, Snackbar.LENGTH_SHORT).show()
                    user_email_te.requestFocus()
                }
                else -> {
                    save_progress.visibility = View.VISIBLE
                    Snackbar.make(requireActivity().findViewById(android.R.id.content), R.string.saving_profile, Snackbar.LENGTH_SHORT).show()
                    editViewModel.save {
                        if(it) {
                            if(editViewModel.hasLocalPicture) {
                                downloadImageFromFirebase("users/${userID}.jpg", userID, true) {
                                    try {
                                        Snackbar.make(requireActivity().findViewById(android.R.id.content), R.string.profile_saved, Snackbar.LENGTH_SHORT).show()
                                        (requireActivity() as MainActivity).refreshNavView()

                                    } catch (e: Exception) {}
                                    findNavController().navigateUp()
                                }
                            }
                            else {
                                try {
                                    Snackbar.make(requireActivity().findViewById(android.R.id.content), R.string.profile_saved, Snackbar.LENGTH_SHORT).show()
                                    (requireActivity() as MainActivity).refreshNavView()

                                } catch (e: Exception) {}
                                findNavController().navigateUp()
                            }

                        }
                    }
                }
            }
            hideKeyboardFrom(requireContext(), requireView())
            true
        }
        else -> {
            (requireActivity() as MainActivity).onNavigateUp()
        }
    }

    //CAMERA
    private fun takePicture() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(requireContext().packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        requireContext(),
                        "com.mad.market.fileprovider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, OPERATION_CAPTURE_PHOTO)
                }
            }
        }
    }

    // Select picture from gallery
    private fun selectPicture() {
        //check permission at runtime
        val checkSelfPermission = ContextCompat.checkSelfPermission(requireContext(),
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (checkSelfPermission != PackageManager.PERMISSION_GRANTED){
            //Requests permissions to be granted to this application at runtime
            ActivityCompat.requestPermissions(requireActivity(),
                arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE), WRITE_EXTERNAL_STORAGE_PERMISSION_CODE)
        }
        else {
            openGallery()
        }
    }

    // Create a temporary image
    @SuppressLint("SimpleDateFormat")
    @Throws(IOException::class)
    fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File = requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            editViewModel.localPicture.value = absolutePath
        }
    }

    private fun openGallery() = Intent(Intent.ACTION_GET_CONTENT).also {
        it.type = "image/*"
        startActivityForResult(it, OPERATION_CHOOSE_PHOTO)
    }

    // Update the current picture by saving a temporary one. This allows to cancel the action if the picture is not wanted
    private fun updateCurrentPhoto(data: Intent?) {
        val uri = data!!.data
        Glide.with(requireContext())
            .asBitmap()
            .load(uri)
            .into(object: CustomTarget<Bitmap>() {
                override fun onLoadCleared(placeholder: Drawable?) {}

                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    editViewModel.user.value!!.propicAngle = 0.0f
                    editViewModel.localPicture.value = saveImage(resource, requireContext(), "temp.jpg")
                    editViewModel.localPicture.value = scaleBitmap(editViewModel.localPicture.value!!, width, height, requireContext())
                    user_propic.setImageBitmap(rotateImage(getRotatedBitmap(editViewModel.localPicture.value!!), editViewModel.user.value!!.propicAngle))
                    rotate_left_button.visibility = View.VISIBLE
                    rotate_right_button.visibility = View.VISIBLE
                }
            })
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>
                                            , grantedResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantedResults)
        when(requestCode){
            WRITE_EXTERNAL_STORAGE_PERMISSION_CODE ->
                if (grantedResults.isNotEmpty() && grantedResults[0] ==
                    PackageManager.PERMISSION_GRANTED){
                    openGallery()
                }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            OPERATION_CAPTURE_PHOTO ->
                if (resultCode == Activity.RESULT_OK) {
                    editViewModel.hasLocalPicture = true
                    editViewModel.user.value!!.propicAngle = 0.0f
                    editViewModel.localPicture.value = scaleBitmap(
                        editViewModel.localPicture.value!!,
                        width,
                        height,
                        requireContext()
                    )
                    user_propic.setImageBitmap(
                        rotateImage(
                            getRotatedBitmap(editViewModel.localPicture.value!!),
                            editViewModel.user.value!!.propicAngle
                        )
                    )
                }
            OPERATION_CHOOSE_PHOTO ->
                if (resultCode == Activity.RESULT_OK) {
                    editViewModel.hasLocalPicture = true
                    updateCurrentPhoto(data)
                }
        }
    }
}
