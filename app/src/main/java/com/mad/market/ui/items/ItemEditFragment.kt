package com.mad.market.ui.items

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.InputFilter
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.format.DateFormat
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.*
import android.widget.TextView.BufferType.EDITABLE
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.ktx.toObject
import com.mad.market.*
import com.mad.market.data.Item
import com.mad.market.data.User
import com.mad.market.exceptions.NoItemException
import com.mad.market.ui.maps.MapsFragment
import com.mad.market.ui.maps.MapsFragment.Companion.RESULT_LATLNG_ID

import kotlinx.android.synthetic.main.fragment_item_edit.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class ItemEditFragment : Fragment() {

    private var itemID: String = ""
    private lateinit var viewModel: ItemEditViewModel

    private val width = 1000
    private val height = 1000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        itemID = arguments?.getString("itemID").orEmpty()
        if(itemID == "") {
            throw NoItemException()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_item_edit, container, false)
    }

    override fun onStart() {
        super.onStart()
        title_item_ie.requestFocus()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //POP-UP MENU
        camera_button.setOnClickListener {
            PopupMenu(context, camera_button).also {
                it.menuInflater.inflate(R.menu.menu_context_camera, it.menu)
                it.setOnMenuItemClickListener { item ->
                    when (item.itemId) {
                        R.id.popup_gallery -> {
                            selectPicture()
                        }
                        R.id.popup_capture_photo -> {
                            takePicture()
                        }
                    }
                    true
                }
                it.show()
            }
        }

        category_item_ie.setOnFocusChangeListener { _, hasFocus ->
            try {
                if(hasFocus)
                {
                    val array = resources.getStringArray(R.array.item_categories)
                    val adapter = ArrayAdapter(requireContext(), R.layout.dropdown_item, array)
                    category_item_ie.setAdapter(adapter)
                    category_item_ie.showDropDown()
                    subcat_item_ie.setText("", TextView.BufferType.EDITABLE)
                }
            } catch (e: Exception) {}
            hideKeyboardFrom(requireContext(), requireView())
        }

        subcat_item_ie.setOnFocusChangeListener { _, hasFocus ->
            try {
                if(hasFocus)
                {
                    val array = when(category_item_ie.text.toString())
                    {
                        resources.getString(R.string.arts_and_crafts) -> resources.getStringArray(R.array.item_category_arts)
                        resources.getString(R.string.sports_and_hobby) -> resources.getStringArray(R.array.item_category_sports)
                        resources.getString(R.string.baby) -> resources.getStringArray(R.array.item_category_baby)
                        resources.getString(R.string.womens_fashion) -> resources.getStringArray(R.array.item_category_women_fashion)
                        resources.getString(R.string.mens_fashion) -> resources.getStringArray(R.array.item_category_men_fashion)
                        resources.getString(R.string.electronics) -> resources.getStringArray(R.array.item_category_electronics)
                        resources.getString(R.string.games_and_videogames) -> resources.getStringArray(R.array.item_category_games)
                        resources.getString(R.string.automotive) -> resources.getStringArray(R.array.item_category_automotive)
                        else -> null
                    }
                    if(array != null)
                    {
                        val adapter = ArrayAdapter(requireContext(), R.layout.dropdown_item, array)
                        subcat_item_ie.setAdapter(adapter)
                        subcat_item_ie.showDropDown()
                    }
                }
            } catch (e: Exception) {}
            hideKeyboardFrom(requireContext(), requireView())
        }



    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this, object: ViewModelProvider.AndroidViewModelFactory(requireActivity().application) {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return ItemEditViewModel(itemID) as T
            }
        }).get(ItemEditViewModel::class.java).apply {
            // get data back from next fragment
            val activity = (requireActivity() as MainActivity)
            val shouldUpdate = activity.getDataFromExternalFragment(MapsFragment.RESULT_LANG_SHOULD_UPDATE) as Boolean?
            if(shouldUpdate != null && shouldUpdate) {
                activity.passDataToExternalFragment(bundleOf(MapsFragment.RESULT_LANG_SHOULD_UPDATE to false))
                val latLng: LatLng = activity.getDataFromExternalFragment(RESULT_LATLNG_ID) as LatLng
                item.value!!.latitude = latLng.latitude
                item.value!!.longitude = latLng.longitude
            }


            // Init itemID to item. If it has already, automatically nothing changes

            newItem.observe(viewLifecycleOwner, Observer {
                if(it) {
                    (requireActivity() as MainActivity).supportActionBar?.title = resources.getString(R.string.new_ad)
                }
                else {
                    (requireActivity() as MainActivity).supportActionBar?.title = resources.getString(R.string.edit_ad)
                }
            })

            item.observe(viewLifecycleOwner, Observer {item ->
                if(item == null) return@Observer
                // Should be always true, but checking is never bad
                if(viewModel.item.value!!.image != "") {
                    viewModel.getImage {
                        image_item_ie.setImageBitmap(rotateImage(getRotatedBitmap(viewModel.localPicture.value!!), viewModel.item.value!!.imageAngle))
                    }
                }

                // If item has been found

                if(!newItem.value!!) {
                    var priceString = String.format("%.2f", item.price)
                    priceString = priceString.replace(",", ".")
                    title_item_ie.setText(item.name, EDITABLE)
                    category_item_ie.setText(item.category, EDITABLE)
                    subcat_item_ie.setText(item.subcategory, EDITABLE)
                    price_item_ie.setText(priceString, EDITABLE)
                    description_item_ie.setText(item.description, EDITABLE)
                    date_item_ie.setText(DateFormat.getDateFormat(requireContext()).format(item.expirationDate), EDITABLE)
                }

                when(item.state) {
                    Item.STATE_ON_SALE -> on_sale_btn.isChecked = true
                    Item.STATE_BLOCKED -> blocked_btn.isChecked = true
                    else -> on_sale_btn.isChecked = true

                }

                // map
                val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
                mapFragment?.getMapAsync { map ->
                    map.clear()
                    map.uiSettings.isScrollGesturesEnabled = false
                    map.setOnMapClickListener {
                        val bundle = bundleOf(MapsFragment.CONTEXT_ID to MapsFragment.CONTEXT_EDIT_AD).apply {
                            if(item.latitude != null && item.longitude != null) {
                                putDouble(MapsFragment.CURRENT_LOCATION_LATUTUDE, item.latitude!!)
                                putDouble(MapsFragment.CURRENT_LOCATION_LONGITUDE, item.longitude!!)
                            }
                        }
                        findNavController().navigate(R.id.action_itemEditFragment_to_mapsFragment, bundle)
                    }
                    // If there is latitude and longitude to be shown
                    if(item.latitude != null && item.longitude != null) {
                        map.addMarker(MarkerOptions().position(LatLng(item.latitude!!, item.longitude!!)).title(getString(R.string.current_position)))
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(item.latitude!!, item.longitude!!), DEFAULT_ZOOM.toFloat()))
                    }
                }
            })

            localPicture.observe(viewLifecycleOwner, Observer {
                if(it == null) {
                    rotate_left_button.visibility = View.GONE
                    rotate_right_button.visibility = View.GONE
                }
                else {
                    rotate_left_button.visibility = View.VISIBLE
                    rotate_right_button.visibility = View.VISIBLE
                    viewModel.item.value!!.image = "${item.value!!.id}.jpg"
                }
            })
            //ROTATION
            rotate_left_button.setOnClickListener{
                item.value!!.imageAngle += 270f
                image_item_ie.setImageBitmap(rotateImage(getRotatedBitmap(localPicture.value!!), item.value!!.imageAngle))
            }
            rotate_right_button.setOnClickListener{
                item.value!!.imageAngle += 90f
                image_item_ie.setImageBitmap(rotateImage(getRotatedBitmap(localPicture.value!!), item.value!!.imageAngle))
            }

            // DATE PICKER
            date_item_ie.setOnClickListener {
                val today = Date()
                var date: Date = try {
                    Date(item.value!!.expirationDate!!)
                } catch (exception: Exception) {
                    today
                }
                if(date < today) {
                    date = today
                }
                val c = Calendar.getInstance()
                c.time = date
                MaterialDatePicker.Builder.datePicker().run {
                    setTitleText(R.string.expiration_date)
                    setSelection(c.timeInMillis)

                    CalendarConstraints.Builder().also {
                        it.setValidator(FutureDateValidator())
                        it.setStart(Date().time)
                        this.setCalendarConstraints(it.build())
                    }

                    build().also {
                        it.addOnPositiveButtonClickListener {mTimeStamp ->
                            Calendar.getInstance().also {
                                it.timeInMillis = mTimeStamp
                                DateFormat.getDateFormat(requireContext()).run {
                                    item.value!!.expirationDate = it.time.time
                                    date_item_ie.text = SpannableStringBuilder(format(it.time))
                                }
                            }
                        }
                        it.show(childFragmentManager, toString())
                    }
                }
            }

            // Set click listeners
            on_sale_btn.setOnClickListener {
                item.value!!.state = Item.STATE_ON_SALE

            }

            blocked_btn.setOnClickListener{
                item.value!!.state = Item.STATE_BLOCKED
            }

            title_item_ie.addTextChangedListener {
                item.value?.name = it.toString()
            }
            category_item_ie.addTextChangedListener {
                item.value?.category = it.toString()
            }

            subcat_item_ie.addTextChangedListener {
                item.value?.subcategory = it.toString()
            }

            price_item_ie.addTextChangedListener {
                try {
                    item.value?.price = it.toString().toFloat()
                } catch (e: Exception) {
                    item.value?.price = null
                }
            }

            description_item_ie.addTextChangedListener {
                item.value?.description = it.toString()
            }
        }
    }

    // Options menu
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        inflater.inflate(R.menu.menu_save, menu)
    }


    // Save
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.menu_save -> {
            // Description is not mandatory, on-sale is the default value
            viewModel.item.value!!.run {
                when {
                    name.isEmpty() -> {
                        Snackbar.make(requireActivity().findViewById(android.R.id.content), R.string.empty_item_name, Snackbar.LENGTH_SHORT).show()
                        title_item_ie.requestFocus()
                    }
                    expirationDate == null -> {
                        Snackbar.make(requireActivity().findViewById(android.R.id.content), R.string.empty_expiration_date, Snackbar.LENGTH_SHORT).show()
                        date_item_ie.requestFocus()
                    }
                    image.isEmpty() -> {
                        Snackbar.make(requireActivity().findViewById(android.R.id.content), R.string.no_image_selected, Snackbar.LENGTH_SHORT).show()
                    }
                    price == null -> {
                        Snackbar.make(requireActivity().findViewById(android.R.id.content), R.string.item_has_no_price, Snackbar.LENGTH_SHORT).show()
                        price_item_ie.requestFocus()
                    }
                    category.isEmpty() -> {
                        Snackbar.make(requireActivity().findViewById(android.R.id.content), R.string.item_has_no_category, Snackbar.LENGTH_SHORT).show()
                        category_item_ie.requestFocus()
                    }
                    subcategory.isEmpty() -> {
                        Snackbar.make(requireActivity().findViewById(android.R.id.content), R.string.item_has_no_subcategory, Snackbar.LENGTH_SHORT).show()
                        subcat_item_ie.requestFocus()
                    }

                    latitude == null || longitude == null -> {
                        Snackbar.make(requireActivity().findViewById(android.R.id.content), R.string.item_has_no_position, Snackbar.LENGTH_SHORT).show()
                    }
                    else -> {
                        save_progress.visibility = View.VISIBLE
                        Snackbar.make(requireActivity().findViewById(android.R.id.content), R.string.saving_item, Snackbar.LENGTH_SHORT).show()
                        viewModel.save {
                            try {
                                Snackbar.make(requireActivity().findViewById(android.R.id.content), R.string.item_saved, Snackbar.LENGTH_SHORT).show()
                                sendBlockedNotifications();
                                findNavController().navigateUp()
                            } catch (e: Exception) {
                                findNavController().navigateUp()
                            }
                        }
                    }
                }
            }
            hideKeyboardFrom(requireContext(), requireView())
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun sendBlockedNotifications() {
        if(viewModel.item.value!!.state == Item.STATE_BLOCKED) {
            for(id in viewModel.item.value!!.interestedPeopleIDs) {
            val title = getString(R.string.item_interested_blocked)
            val message = "${viewModel.item.value!!.name} ${getString(R.string.has_been_blocked)}"
            database.collection("users").document(id).get().addOnSuccessListener { snapshot ->
                    val to = snapshot.toObject<User>()
                    to?.token?.let { sendNotification(it, title, message) }
                }
            }
        }
    }

    //CAMERA
    private fun takePicture() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(requireContext().packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        requireContext(),
                        "com.mad.market.fileprovider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, OPERATION_CAPTURE_PHOTO)
                }
            }
        }
    }

    // Select picture from gallery
    private fun selectPicture() {
        //check permission at runtime
        val checkSelfPermission = ContextCompat.checkSelfPermission(requireContext(),
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (checkSelfPermission != PackageManager.PERMISSION_GRANTED){
            //Requests permissions to be granted to this application at runtime
            ActivityCompat.requestPermissions(requireActivity(),
                arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE), WRITE_EXTERNAL_STORAGE_PERMISSION_CODE)
        }
        else {
            openGallery()
        }
    }

    // Create a temporary image
    @SuppressLint("SimpleDateFormat")
    @Throws(IOException::class)
    fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File = requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            viewModel.localPicture.value = absolutePath
        }
    }

    private fun openGallery() = Intent(Intent.ACTION_GET_CONTENT).also {
        it.type = "image/*"
        startActivityForResult(it, OPERATION_CHOOSE_PHOTO)
    }

    // Update the current picture by saving a temporary one. This allows to cancel the action if the picture is not wanted
    private fun updateCurrentPhoto(data: Intent?) {
        val uri = data!!.data
        Glide.with(requireContext())
            .asBitmap()
            .load(uri)
            .into(object: CustomTarget<Bitmap>() {
                override fun onLoadCleared(placeholder: Drawable?) {}

                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    viewModel.item.value!!.imageAngle = 0.0f
                    viewModel.localPicture.value = saveImage(resource, requireContext(), "temp.jpg")
                    viewModel.localPicture.value = scaleBitmap(viewModel.localPicture.value!!, width, height, requireContext())
                    image_item_ie.setImageBitmap(rotateImage(getRotatedBitmap(viewModel.localPicture.value!!), viewModel.item.value!!.imageAngle))
                    rotate_left_button.visibility = View.VISIBLE
                    rotate_right_button.visibility = View.VISIBLE
                }
            })
    }



    // On request permission
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>
                                            , grantedResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantedResults)
        when(requestCode){
            WRITE_EXTERNAL_STORAGE_PERMISSION_CODE ->
                if (grantedResults.isNotEmpty() && grantedResults[0] ==
                    PackageManager.PERMISSION_GRANTED){
                    openGallery()
                }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            OPERATION_CAPTURE_PHOTO ->
                if (resultCode == Activity.RESULT_OK) {
                    viewModel.hasLocalPicture = true
                    viewModel.item.value!!.imageAngle = 0.0f
                    viewModel.localPicture.value = scaleBitmap(
                        viewModel.localPicture.value!!,
                        width,
                        height,
                        requireContext()
                    )
                    image_item_ie.setImageBitmap(
                        rotateImage(
                            getRotatedBitmap(viewModel.localPicture.value!!),
                            viewModel.item.value!!.imageAngle
                        )
                    )
                }
            OPERATION_CHOOSE_PHOTO ->
                if (resultCode == Activity.RESULT_OK) {
                    viewModel.hasLocalPicture = true
                    updateCurrentPhoto(data)
                }
        }
    }

}
