package com.mad.market.ui.items

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.mad.market.MainActivity
import com.mad.market.R
import com.mad.market.data.Rating
import com.mad.market.exceptions.NoItemException
import com.mad.market.getCurrentUserID
import com.mad.market.ui.profile.ProfileViewModel
import kotlinx.android.synthetic.main.fragment_rating_user.*


class RatingUserFragment : Fragment() {
    private lateinit var itemID: String
    private lateinit var authorID: String
    private lateinit var viewModel: ProfileViewModel
    private lateinit var itemViewModel: ItemViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        itemID = arguments?.getString("itemID").orEmpty()
        authorID = arguments?.getString("authorID").orEmpty()
        if(itemID == "") {
            throw NoItemException()
        }
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_rating_user, container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        inflater.inflate(R.menu.menu_save, menu)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProvider(this, object: ViewModelProvider.AndroidViewModelFactory(requireActivity().application) {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return ProfileViewModel(authorID, activity = requireActivity() as MainActivity) as T
            }
        }).get(ProfileViewModel::class.java)

        itemViewModel = ViewModelProvider(this, object: ViewModelProvider.AndroidViewModelFactory(requireActivity().application) {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return ItemViewModel(itemID) as T
            }
        }).get(ItemViewModel::class.java)

        rating_bar.setOnClickListener {
            rating_bar.rating = rating_bar.rating

            if (rating_bar.rating >= 5.0){
                complaint_tv.text = getString(R.string.rate_5)
            }
            if (rating_bar.rating >= 4.0 && rating_bar.rating < 5.0){
                complaint_tv.text = getString(R.string.rate_4)
            }
            if (rating_bar.rating >= 3.0 && rating_bar.rating < 4.0){
                complaint_tv.text = getString(R.string.rate_3)
            }
            if (rating_bar.rating < 3){
                complaint_tv.text = getString(R.string.rate_low)
            }
        }
    }


    //Save
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.menu_save -> {
            if(rating_bar.rating <= 0) {
                Snackbar.make(requireActivity().findViewById(android.R.id.content), R.string.empty_rating, Snackbar.LENGTH_SHORT).show()
                rating_bar.requestFocus()
            }
            else {
                viewModel.user.value!!.ratingList.add(Rating(
                    feedback = edit_feedback.text.toString(),
                    rate = rating_bar.rating,
                    from = getCurrentUserID().orEmpty(),
                    forItem = itemViewModel.item.value!!.id
                ))
                viewModel.save{
                    itemViewModel.item.value!!.rated = true
                    itemViewModel.save {
                        try{
                            Snackbar.make(requireActivity().findViewById(android.R.id.content), R.string.rateDone, Snackbar.LENGTH_SHORT).show()
                        }
                        catch (E: Exception){}
                        findNavController().navigateUp();
                    }
                }
            }
            true
        }
        else -> super.onOptionsItemSelected(item)
    }
}