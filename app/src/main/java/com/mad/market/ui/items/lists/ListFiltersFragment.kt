package com.mad.market.ui.items.lists

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ArrayAdapter
import android.widget.SeekBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.crystal.crystalrangeseekbar.interfaces.OnSeekbarChangeListener
import com.google.android.material.snackbar.Snackbar
import com.mad.market.R
import com.mad.market.data.Filters
import com.mad.market.hideKeyboardFrom
import kotlinx.android.synthetic.main.fragment_list_filters.*
import kotlinx.android.synthetic.main.fragment_list_filters.view.*


class ListFiltersFragment : Fragment() {

    private lateinit var viewModel: ListFiltersViewModel
    private var filter: Filters = Filters()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_filters, container, false)

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        inflater.inflate(R.menu.menu_save, menu)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val maxVal = "500"
        super.onViewCreated(view, savedInstanceState)
        //init viewModel
        viewModel = ViewModelProvider(requireActivity()).get(ListFiltersViewModel::class.java)
        //show range seek bar and min-max text value for price
        rangeSeekbar.setOnRangeSeekbarChangeListener { minValue, maxValue ->
            minPrice.text = minValue.toString()
            if (maxValue.toString() == maxVal) {
                maxPrice.text = "500+"
            }
            else {
                maxPrice.text = maxValue.toString()
            }
        }
        initFilters()
        //show categories
        category_item_cf.setOnFocusChangeListener { _, hasFocus ->
            try {
                if(hasFocus)
                {
                    val array = resources.getStringArray(R.array.item_categories)
                    val adapter = ArrayAdapter(requireContext(), R.layout.dropdown_item, array)
                    category_item_cf.setAdapter(adapter)
                    category_item_cf.showDropDown()
                    subcat_item_cf.setText("", TextView.BufferType.EDITABLE)
                }
            } catch (e: Exception) {}
            hideKeyboardFrom(requireContext(), requireView())
        }
        //show subcategories according to category
        subcat_item_cf.setOnFocusChangeListener { _, hasFocus ->
            try {
                if(hasFocus)
                {
                    val array = when(category_item_cf.text.toString())
                    {
                        resources.getString(R.string.arts_and_crafts) -> resources.getStringArray(R.array.item_category_arts)
                        resources.getString(R.string.sports_and_hobby) -> resources.getStringArray(R.array.item_category_sports)
                        resources.getString(R.string.baby) -> resources.getStringArray(R.array.item_category_baby)
                        resources.getString(R.string.womens_fashion) -> resources.getStringArray(R.array.item_category_women_fashion)
                        resources.getString(R.string.mens_fashion) -> resources.getStringArray(R.array.item_category_men_fashion)
                        resources.getString(R.string.electronics) -> resources.getStringArray(R.array.item_category_electronics)
                        resources.getString(R.string.games_and_videogames) -> resources.getStringArray(R.array.item_category_games)
                        resources.getString(R.string.automotive) -> resources.getStringArray(R.array.item_category_automotive)
                        else -> null
                    }
                    if(array != null)
                    {
                        val adapter = ArrayAdapter(requireContext(), R.layout.dropdown_item, array)
                        subcat_item_cf.setAdapter(adapter)
                        subcat_item_cf.showDropDown()
                    }
                }
            } catch (e: Exception) {}
            hideKeyboardFrom(requireContext(), requireView())
        }

        clear_categories_btn.setOnClickListener {
            subcat_item_cf.setText("", TextView.BufferType.EDITABLE)
            category_item_cf.setText("", TextView.BufferType.EDITABLE)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.menu_save -> {
            createFilter()
            viewModel.updateData(filter)
            Snackbar.make(requireActivity().findViewById(android.R.id.content), R.string.filters_applied, Snackbar.LENGTH_SHORT).show()
            findNavController().navigateUp()
            hideKeyboardFrom(requireContext(), requireView())
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun initFilters(){
        val filter = viewModel.filters.value
        Log.e("filters", filter.toString())
        if (filter?.maxPrice!! != Int.MAX_VALUE) {
            rangeSeekbar.setMaxStartValue(filter.maxPrice.toFloat()).apply();
            maxPrice.text = filter.maxPrice.toString()
        }
        if (filter.category != ""){
            category_item_cf.setText(filter.category)
        }
        if (filter.subCategory != ""){
            subcat_item_cf.setText(filter.subCategory)
        }
        seekBar.progress = filter.distance.toInt()
        if(seekBar.progress != seekBar!!.max) {
            distance_tv.text = "${seekBar.progress.toString()} Km"
        }
        else {
            distance_tv.text = getString(R.string.anywhere)
        }

        val listener = object: SeekBar.OnSeekBarChangeListener {
            @SuppressLint("SetTextI18n")
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if(progress != seekBar!!.max) {
                    distance_tv.text = "${progress.toString()} Km"
                }
                else {
                    distance_tv.text = getString(R.string.anywhere)
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }
        }
        seekBar.setOnSeekBarChangeListener(listener)
    }

    private fun createFilter(){
        filter.minPrice = minPrice.text.toString().toInt()
        if (maxPrice.text.toString() != "500+") {
            filter.maxPrice = maxPrice.text.toString().toInt()
        }
        else {
            filter.maxPrice = Int.MAX_VALUE
        }
        filter.category = category_item_cf.text.toString()
        filter.subCategory = subcat_item_cf.text.toString()
        filter.distance = seekBar.progress.toDouble()
    }
}
