package com.mad.market.ui.items.lists

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.mad.market.MainActivity
import com.mad.market.R
import com.mad.market.data.Item
import com.mad.market.getCurrentUserID
import com.mad.market.ui.adapters.ItemAdapter
import kotlinx.android.synthetic.main.fragment_item_list.*

class ItemListFragment : Fragment() {

    lateinit var viewModel: ItemsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_item_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fab.setOnClickListener {
            (requireActivity() as MainActivity).getCurrentUser().also {
                val itemID = "${getCurrentUserID()}-${it.nextItemID}"
                it.nextItemID++
                it.save(null) {
                    val bundle = bundleOf("itemID" to itemID)
                    findNavController().navigate(R.id.action_itemListFragment_to_itemEditFragment, bundle)
                }
            }
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val fragment = this
        viewModel = ViewModelProvider(requireActivity()).get(ItemsViewModel::class.java).apply {
            items.observe(viewLifecycleOwner, Observer {
                if(it == null) return@Observer
                val tempItems = mutableListOf<Item>()
                for(item in items.value!!) {
                    if(item.authorID == getCurrentUserID()) {
                        tempItems.add(item)
                    }
                }
                finalItems.value = tempItems
            })
            finalItems.observe(viewLifecycleOwner, Observer {
                if(it == null) return@Observer
                if(it.isEmpty()) {
                    recycler_view.visibility = View.GONE
                    no_ads_tv.visibility = View.VISIBLE
                }
                else {
                    recycler_view.visibility = View.VISIBLE
                    no_ads_tv.visibility = View.GONE
                    recycler_view.adapter = ItemAdapter(finalItems.value!!, fragment)
                    recycler_view.layoutManager = LinearLayoutManager(fragment.requireActivity())
                }
            })
        }
    }

}
