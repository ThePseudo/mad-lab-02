package com.mad.market.ui.profile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.firestore.ktx.toObject
import com.mad.market.data.User
import com.mad.market.*
import kotlin.properties.Delegates

class ProfileViewModel(var userID: String? = null, userEmail: String? = null, var activity: MainActivity) : ViewModel() {
    var user: MutableLiveData<User> = MutableLiveData(User())
    var shouldShowMoreInfo by Delegates.notNull<Boolean>()
    private var userListener: ListenerRegistration? = null
    var shouldRegisterOrResetTrigger: MutableLiveData<Boolean> = MutableLiveData(false)
    var alreadyTriggeredFragment = false

    init {
        if((userID == null).or(userID == "")) {
            userID = login(activity)?.uid
        }
        if(userID != null) {
            shouldShowMoreInfo = when(userID) {
                FirebaseAuth.getInstance().currentUser?.uid.orEmpty() -> true
                else -> false
            }
            if (userEmail != null) user.value!!.email = userEmail
            userListener = database.collection("users").document(userID!!).addSnapshotListener { snapshot, e ->
                if(e == null) {
                    if(snapshot != null && snapshot.exists()) {
                        user.value = snapshot.toObject<User>()
                    }
                    else {
                        shouldRegisterOrResetTrigger.value = true
                    }
                }
            }
        }
    }

    fun reset(onSuccess: (user: User?) -> Unit = {}) {
        if((userID == null).or(userID == "")) userID = getCurrentUserID()
        shouldShowMoreInfo = when(userID) {
            getCurrentUserID() -> true
            else -> false
        }
        userListener?.remove()
        userListener = database.collection("users").document(userID!!).addSnapshotListener { snapshot, e ->
            if (e == null) {
                if (snapshot != null && snapshot.exists()) {
                    user.value = snapshot.toObject<User>()
                    onSuccess(user.value)
                    shouldRegisterOrResetTrigger.value = true
                    shouldRegisterOrResetTrigger.value = false
                }
                else {
                    shouldRegisterOrResetTrigger.value = true
                }
            }
        }
    }

    fun save(onSuccess : (success: Boolean) -> Unit = {}) {
        if(userID == null) userID = getCurrentUserID().orEmpty()
        user.value!!.save(null, onSuccess)
    }

}
