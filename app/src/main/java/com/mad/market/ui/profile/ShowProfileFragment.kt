package com.mad.market.ui.profile

import android.R.attr.button
import android.annotation.SuppressLint
import android.graphics.Paint
import android.os.Bundle
import android.text.format.DateFormat
import android.view.*
import android.widget.LinearLayout
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.mad.market.*
import com.mad.market.ui.maps.MapsFragment.Companion.CONTEXT_ID
import com.mad.market.ui.maps.MapsFragment.Companion.CONTEXT_ONLY_VIEW
import com.mad.market.ui.maps.MapsFragment.Companion.CURRENT_LOCATION_LATUTUDE
import com.mad.market.ui.maps.MapsFragment.Companion.CURRENT_LOCATION_LONGITUDE
import kotlinx.android.synthetic.main.fragment_show_profile.*


class ShowProfileFragment : Fragment() {

    private lateinit var profileID: String
    private lateinit var viewModel: ProfileViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        profileID = arguments?.getString("profileID").orEmpty()
        if(profileID == "") {
            profileID = getCurrentUserID().orEmpty()
        }
        return inflater.inflate(R.layout.fragment_show_profile, container, false)

    }

    @SuppressLint("SetTextI18n")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)
        viewModel = ViewModelProvider(this, object: ViewModelProvider.AndroidViewModelFactory(requireActivity().application) {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return ProfileViewModel(profileID, activity = requireActivity() as MainActivity) as T
            }
        }).get(ProfileViewModel::class.java)
        viewModel.run {
            user.observe(viewLifecycleOwner)  { curUser ->
                if (curUser!!.hasPropic) {
                    downloadImageFromFirebase("users/${profileID}.jpg", profileID) {
                        try { // if we switch screen too fast
                            user_propic.setImageBitmap(
                                rotateImage(
                                    it?.let { it1 -> getRotatedBitmap(it1) },
                                    curUser.propicAngle
                                )
                            )
                        } catch (e: NullPointerException) {}
                    }
                }
                user_name.text = curUser.fullName
                user_nickname.text = curUser.nickName
                user_email.text = curUser.email
                val numVotes = viewModel.user.value!!.ratingList.size
                avgVotesText.text = "$numVotes"
                avgVotesText.paintFlags = avgVotesText.paintFlags or Paint.UNDERLINE_TEXT_FLAG
                avgVotesText.setOnClickListener{
                    val bundle = bundleOf("userID" to profileID)
                    findNavController().navigate(R.id.action_showProfileFragment_to_ratingListFragment, bundle)
                }
                rBar.rating = averageRating()
                val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
                mapFragment?.getMapAsync {
                    it.clear()
                    it.uiSettings.isScrollGesturesEnabled = false
                    if(curUser.latitude != null && curUser.longitude != null) {
                        mapFragment.requireView().visibility = View.VISIBLE
                        it.addMarker(MarkerOptions().position(LatLng(curUser.latitude!!, curUser.longitude!!)).title(getString(R.string.current_position)))
                        it.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(curUser.latitude!!, curUser.longitude!!), DEFAULT_ZOOM.toFloat()))
                        (mapFragment.requireView().parent as LinearLayout).background = requireActivity().getDrawable(R.drawable.border_square)
                        it.setOnMapClickListener {
                            val bundle = bundleOf(CONTEXT_ID to CONTEXT_ONLY_VIEW).apply {
                                putDouble(CURRENT_LOCATION_LATUTUDE, curUser.latitude!!)
                                putDouble(CURRENT_LOCATION_LONGITUDE, curUser.longitude!!)
                            }
                            findNavController().navigate(R.id.action_showProfileFragment_to_mapsFragment, bundle)
                        }
                    }
                    else {
                        mapFragment.requireView().visibility = View.GONE
                        (mapFragment.requireView().parent as LinearLayout).background = null
                    }
                }

                user_type.text = "${getString(R.string.Private)} ${getString(R.string.user)}"
                if (curUser.userType != "") {
                    user_type.text = "${curUser.userType} ${resources.getString(R.string.user)}"
                }

                // Privacy settings: if should show:
                if(shouldShowMoreInfo) {
                    try {
                        user_birthday.text =
                            DateFormat.getDateFormat(requireContext()).format(curUser.birthday)
                    } catch (e: Exception) { }

                    user_phone.text = curUser.phoneNumber
                    if (curUser.userType == "Business") {
                        user_type.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            R.drawable.business_user_small,
                            0,
                            0,
                            0
                        )
                    } else {
                        user_type.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            R.drawable.private_user_small,
                            0,
                            0,
                            0
                        )
                    }
                }
                // Privacy settings: if should not show
                else {
                    user_phone.visibility = View.GONE
                    user_birthday.visibility = View.GONE
                }
            }
        }

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        if(profileID == getCurrentUserID()) {
            inflater.inflate(R.menu.menu_edit, menu)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.edit -> {
            findNavController().navigate(R.id.action_showProfile_to_editProfileFragment)
            true
        }
        else -> super.onOptionsItemSelected(item)
    }
    private fun averageRating (): Float = run {
        var avgRate = 0.0f
        viewModel.user.value!!.ratingList.forEach {
            avgRate += it.rate
        }
        avgRate /= viewModel.user.value!!.ratingList.size;
        return avgRate
    }
}
