package com.mad.market.ui.maps

import android.location.Location
import androidx.lifecycle.ViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.maps.GoogleMap
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker

class MapsViewModel : ViewModel() {
    lateinit var map: GoogleMap
    lateinit var placesClient: PlacesClient
    lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    var locationPermissionGranted = false
    var lastKnownLocation: Location? = null
    var selectedLatLng: LatLng? = null
    var currentMarker: Marker? = null
    var shouldShowDistance: Boolean = false
    var firstTime: Boolean = true
}