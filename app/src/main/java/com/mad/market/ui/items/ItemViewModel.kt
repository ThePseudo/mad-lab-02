package com.mad.market.ui.items

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.ktx.toObject
import com.mad.market.data.Item
import com.mad.market.*
import com.mad.market.data.User

class ItemViewModel(var itemID: String? = null) : ViewModel() {
    var item: MutableLiveData<Item> = MutableLiveData(Item())
    var itemAuthor: MutableLiveData<User> = MutableLiveData(User())
    var interestedPeopleNames: MutableLiveData<MutableList<User>> = MutableLiveData(mutableListOf())
    private var oldInterestedPeopleNumber = -1

    init {
        database.collection("items").document(itemID!!).addSnapshotListener {snapshot, e ->
            if(e != null) {
                Log.e("Item", "Retrivial error")
                return@addSnapshotListener
            }
            if(snapshot != null && snapshot.exists()) {
                item.value = snapshot.toObject<Item>()
                // get user
                database.collection("users")
                    .document(item.value!!.authorID)
                    .get().addOnSuccessListener {
                        itemAuthor.value = it.toObject<User>()
                    }

                // get list of interested user nicknames
                // don't if size has not changed
                if(item.value!!.interestedPeopleIDs.size != oldInterestedPeopleNumber) {
                    // Reset list of names
                    val tempList = mutableListOf<User>()
                    interestedPeopleNames.value = mutableListOf()
                    oldInterestedPeopleNumber = item.value!!.interestedPeopleIDs.size
                    for (userID in item.value!!.interestedPeopleIDs) {
                        // Add to list of names (only once tho)
                        database.collection("users").document(userID).get().addOnSuccessListener {
                            val person = it.toObject<User>()
                            if (person != null) {
                                tempList.add(person)
                                interestedPeopleNames.value = null
                                interestedPeopleNames.value = tempList
                            }
                        }
                    }
                }

            }
        }
    }

    fun save(thenRun: () -> Unit) {
        database.collection("items").document(itemID!!)
            .set(item.value!!).addOnSuccessListener {
                thenRun()
            }
    }

}
