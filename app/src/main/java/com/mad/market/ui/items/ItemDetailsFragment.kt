package com.mad.market.ui.items

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.format.DateFormat
import android.view.*
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.lifecycle.*
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import com.mad.market.*
import com.mad.market.R
import kotlinx.android.synthetic.main.fragment_item_details.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.toObject
import com.mad.market.data.Item
import com.mad.market.data.User
import com.mad.market.exceptions.NoItemException
import com.mad.market.ui.adapters.NamesAdapter
import com.mad.market.ui.maps.MapsFragment
import com.mad.market.ui.profile.ProfileViewModel

class ItemDetailsFragment : Fragment() {

    private lateinit var itemID: String
    private lateinit var authorID: String
    private lateinit var viewModel: ItemViewModel
    private lateinit var viewModelP : ProfileViewModel
    //private lateinit var viewModelProfile : ProfileViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        itemID = arguments?.getString("itemID").orEmpty()
        authorID = arguments?.getString("authorID").orEmpty()
        if(itemID == "") {
            throw NoItemException()
        }
        return inflater.inflate(R.layout.fragment_item_details, container, false)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.edit -> {
            val bundle = bundleOf("itemID" to itemID)
            findNavController().navigate(R.id.action_itemDetailsFragment_to_itemEditFragment, bundle)
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        if(getCurrentUserID() == authorID)
            inflater.inflate(R.menu.menu_edit, menu)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        show_profile_button.setOnClickListener {
            val bundle = bundleOf("profileID" to authorID)
            findNavController().navigate(R.id.action_itemDetailsFragment_to_showProfileFragment, bundle)
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)
        interested_people_list.layoutManager = LinearLayoutManager(this.requireActivity())
        viewModelP = ViewModelProvider(requireActivity()).get(ProfileViewModel::class.java)
        viewModel = ViewModelProvider(this, object: ViewModelProvider.AndroidViewModelFactory(requireActivity().application) {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return ItemViewModel(itemID) as T
            }
        }).get(ItemViewModel::class.java)
        viewModel.run {
            item.observe(viewLifecycleOwner)  { curItem ->
                if(item.value!!.id == "") return@observe
                if (curItem.image != "") {
                    downloadImageFromFirebase("items/${itemID}.jpg", item.value!!.image, false) {
                        try { // if we switch screen too fast
                            image_item_id.setImageBitmap(
                                rotateImage(
                                    it?.let { it1 -> getRotatedBitmap(it1) },
                                    curItem.imageAngle
                                )
                            )
                        } catch (e: NullPointerException) {}
                    }
                }
                if(curItem.state == Item.STATE_SOLD) {
                    setHasOptionsMenu(false)
                }
                title_item_id.text = curItem.name
                category_item_id.text = curItem.category
                subcat_item_id.text = curItem.subcategory
                price_item_id.text = "%.2f".format(curItem.price)

                val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
                mapFragment?.getMapAsync {
                    it.clear()
                    it.uiSettings.isScrollGesturesEnabled = false
                    if(curItem.latitude != null && curItem.longitude != null) {
                        mapFragment.requireView().visibility = View.VISIBLE
                        it.addMarker(MarkerOptions().position(LatLng(curItem.latitude!!, curItem.longitude!!)).title(getString(R.string.current_position)))
                        it.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(curItem.latitude!!, curItem.longitude!!), DEFAULT_ZOOM.toFloat()))
                        (mapFragment.requireView().parent as LinearLayout).background = requireActivity().getDrawable(R.drawable.border_square)
                        it.setOnMapClickListener {
                            val bundle = bundleOf(MapsFragment.CONTEXT_ID to MapsFragment.CONTEXT_SHOW_DISTANCE).apply {
                                putDouble(MapsFragment.CURRENT_LOCATION_LATUTUDE, item.value!!.latitude!!)
                                putDouble(MapsFragment.CURRENT_LOCATION_LONGITUDE, item.value!!.longitude!!)
                            }
                            findNavController().navigate(R.id.action_itemDetailsFragment_to_mapsFragment, bundle)
                        }
                    }
                }

                description_item_id.text = curItem.description
                if(authorID == getCurrentUserID() || curItem.state != Item.STATE_ON_SALE) {
                    fab.visibility = View.GONE
                }
                else {
                    interested_people_tv.visibility = View.GONE
                    interested_people_list.visibility = View.GONE
                    if(viewModel.item.value!!.interestedPeopleIDs.contains(getCurrentUserID())) {
                        fab.setImageResource(R.drawable.ic_x_black_24dp)
                    }
                    else {
                        fab.setImageResource(R.drawable.ic_check_black_24dp)
                    }
                }
                when(item.value!!.state) {
                    Item.STATE_SOLD -> {
                        expiration_date_info_tv.visibility = View.INVISIBLE
                        date_item_id.setText(R.string.sold)
                        date_item_id.setTextColor(ContextCompat.getColor(requireContext(), android.R.color.holo_red_dark))
                    }
                    Item.STATE_BLOCKED -> {
                        expiration_date_info_tv.visibility = View.INVISIBLE
                        date_item_id.setText(R.string.blocked)
                        date_item_id.setTextColor(ContextCompat.getColor(requireContext(), android.R.color.holo_red_dark))
                    }
                    else -> {
                        date_item_id.setTextColor(ContextCompat.getColor(requireContext(), android.R.color.black))
                        if(curItem.expired()) {
                            date_item_id.setTextColor(ContextCompat.getColor(requireContext(), android.R.color.holo_red_dark))
                        }
                        date_item_id.text = DateFormat.getDateFormat(requireContext()).format(curItem.expirationDate)
                    }
                }
            }
            itemAuthor.observe(viewLifecycleOwner) {
                if(it == null) return@observe
                if(it.nickName.isNotEmpty())
                    author_tv.text = viewModel.itemAuthor.value!!.nickName
            }

            interestedPeopleNames.observe(viewLifecycleOwner) {
                if(it == null) return@observe
                interested_people_list.adapter = NamesAdapter(interestedPeopleNames.value!!, item.value!!, this@ItemDetailsFragment)
                if(interestedPeopleNames.value!!.size == 0) {
                    interested_people_tv.visibility = View.GONE
                    interested_people_list.visibility = View.GONE
                }
                else {
                    if(item.value?.authorID == getCurrentUserID()) {
                        interested_people_tv.visibility = View.VISIBLE
                        interested_people_list.visibility = View.VISIBLE
                    }
                }
            }
        }

        fab.setOnClickListener {
            if(viewModel.item.value!!.interestedPeopleIDs.contains(getCurrentUserID())) {
                viewModel.item.value!!.interestedPeopleIDs.remove(getCurrentUserID())
                //devo andare nella mia lista di item a cui sono interessato e rimuovere questo item
                viewModelP.user.value!!.interestedItems.remove(viewModel.item.value!!.id)
                viewModelP.save()
                viewModel.save {
                    Snackbar.make(requireActivity().findViewById(android.R.id.content), R.string.unfollow_ad, Snackbar.LENGTH_SHORT).show()
                }
            }
            else {
                viewModel.item.value!!.interestedPeopleIDs.add(getCurrentUserID().orEmpty())
                //devo andare nella mia lista di item a cui sono interessato e aggiungere questo item
                viewModelP.user.value!!.interestedItems.add(viewModel.item.value!!.id)
                viewModelP.save()
                viewModel.save {
                    Snackbar.make(requireActivity().findViewById(android.R.id.content), R.string.follow_ad, Snackbar.LENGTH_SHORT).show()
                    sendInterestedNotification()
                }
            }
        }

    }

    private fun sendInterestedNotification() {
        val title = getString(R.string.someone_interested)
        val nickname = (requireActivity() as MainActivity).profileViewModel.user.value!!.nickName
        val itemName = viewModel.item.value!!.name
        val message = "$nickname ${getString(R.string.is_interested_in)} $itemName!"
        database.collection("users").document(viewModel.item.value!!.authorID).get().addOnSuccessListener {
            val user = it.toObject<User>()
            if(user != null && user.token.isNotEmpty()) {
                sendNotification(user.token, title, message)
            }
        }
    }

}
