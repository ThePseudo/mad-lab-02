package com.mad.market.ui.items.lists

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.firestore.ktx.toObject
import com.mad.market.data.Item
import com.mad.market.database

class ItemsViewModel : ViewModel() {
    var items = MutableLiveData<MutableList<Item>>(mutableListOf())
    var finalItems = MutableLiveData<MutableList<Item>>(mutableListOf())
    var fusedLocationProviderClient: FusedLocationProviderClient? = null
    var currentLocation: LatLng? = null

    init {
        database.collection("items").addSnapshotListener { snapshot, e ->
            if(e != null) {
                Log.e("On sale error", e.message)
                return@addSnapshotListener
            }
            if(snapshot != null) {
                val tempItems = mutableListOf<Item>()
                for(itemDocument in snapshot.documents) {
                    var item: Item
                    try {
                        item = itemDocument.toObject<Item>()!!
                    } catch (e: Exception) {
                        return@addSnapshotListener
                    }
                    if(item != Item()) {
                        tempItems.add(item)
                    }
                }
                items.value = tempItems
            }
        }
    }

}