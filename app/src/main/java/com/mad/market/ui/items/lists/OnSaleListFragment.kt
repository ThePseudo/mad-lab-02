package com.mad.market.ui.items.lists

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuth
import com.mad.market.*
import com.mad.market.data.Filters
import com.mad.market.data.Item
import com.mad.market.ui.adapters.ItemAdapter
import com.mad.market.ui.profile.ProfileViewModel
import kotlinx.android.synthetic.main.fragment_item_on_sale.*
import java.util.*

class OnSaleListFragment : Fragment() {

    lateinit var listViewModel: ItemsViewModel
    private lateinit var viewModel: ListFiltersViewModel
    private lateinit var profileViewModel: ProfileViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        inflater.inflate(R.menu.menu_show_filters, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.filters -> {
            findNavController().navigate(R.id.action_onSaleListFragment_to_listFilters)
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_item_on_sale, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recycler_view.layoutManager = LinearLayoutManager(this.requireActivity())

        viewModel = ViewModelProvider(requireActivity()).get(ListFiltersViewModel::class.java)
        profileViewModel = ViewModelProvider(requireActivity(), object: ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return ProfileViewModel(activity = requireActivity() as MainActivity) as T
            }
        }).get(ProfileViewModel::class.java).apply {
            shouldRegisterOrResetTrigger.observe(viewLifecycleOwner, Observer {
                if(it) {
                    if(profileViewModel.user.value != null
                        && profileViewModel.user.value!!.fullName != "") {
                        filterAndPopulate(viewModel.filters, listViewModel.items, title_search_text.text.toString())
                    }
                    else if(!alreadyTriggeredFragment){
                        alreadyTriggeredFragment = true
                        findNavController().navigate(R.id.action_onSaleListFragment_to_editProfileFragment) // (if you like seeing your profile again move up)
                    }

                }
            })
        }
        val fragment = this
        listViewModel = ViewModelProvider(requireActivity()).get(ItemsViewModel::class.java)
        listViewModel.apply {
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(requireContext())
            checkPermissions()
            items.observe(viewLifecycleOwner, Observer {
                    filterAndPopulate(viewModel.filters, listViewModel.items, title_search_text.text.toString())
                })

            finalItems.observe(viewLifecycleOwner, Observer {
                    if(finalItems.value!!.isEmpty()) {
                        recycler_view.visibility = GONE
                        no_ads_on.visibility = VISIBLE
                        image_no_ads.visibility = VISIBLE
                    }
                    else {
                        // bind list to data
                        recycler_view.visibility = VISIBLE
                        no_ads_on.visibility = GONE
                        image_no_ads.visibility = GONE
                    }
                    recycler_view.adapter = ItemAdapter(listViewModel.finalItems.value!!, fragment)
                })
        }


        search_button.setOnClickListener{
            filterAndPopulate(viewModel.filters, listViewModel.items, title_search_text.text.toString())
        }

    }

    private fun filterAndPopulate(
        filters: MutableLiveData<Filters>,
        items: MutableLiveData<MutableList<Item>>,
        textSearch: String
    ) {
        Log.e("filters", filters.value.toString())
        Log.e("items", items.value.toString())
        Log.e("title", textSearch)

        val finalItems = mutableListOf<Item>()
        listViewModel.finalItems.value = mutableListOf()

        for (item in items.value!!){
            if(getCurrentUserID() == null) break // if no user, no items to be shown
            if(item.authorID != getCurrentUserID()) {
                if(!item.expired() && item.state == Item.STATE_ON_SALE) {
                    if (textSearch.isEmpty() || item.name.toLowerCase(Locale.ROOT).contains(textSearch.toLowerCase(Locale.ROOT))) {
                        if (item.price!! > filters.value?.minPrice!! && item.price!! < filters.value?.maxPrice!!) {
                            if (filters.value?.category == "" || item.category == filters.value!!.category) {
                                if(filters.value?.subCategory  == "" || item.subcategory == filters.value!!.subCategory){
                                    if (filters.value?.location == "" || item.location == filters.value!!.location){
                                        if(listViewModel.currentLocation != null) {
                                            val distance = distanceInKm(listViewModel.currentLocation!!, LatLng(item.latitude!!, item.longitude!!))
                                            if(filters.value?.distance!! > 999.toDouble()) {
                                                finalItems.add(item)
                                            }
                                            else if(filters.value?.distance!! > distance) {
                                                finalItems.add(item)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        listViewModel.finalItems.value = finalItems
        Log.e("Final items", listViewModel.finalItems.value!!.toString())
    }

    @SuppressLint("MissingPermission")
    fun getDeviceLocation() {
        listViewModel.fusedLocationProviderClient!!.lastLocation.apply {
            addOnCompleteListener(requireActivity()) {
                if(it.isSuccessful) {
                    if(it.result != null) {
                        listViewModel.currentLocation = LatLng(it.result!!.latitude, result!!.longitude)
                        filterAndPopulate(viewModel.filters, listViewModel.items, title_search_text.text.toString())
                    }
                }
            }
        }
    }

    private fun checkPermissions() {
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION), LOCATION_PERMISSION_CODE
            )
        }
        else {
            getDeviceLocation()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            getDeviceLocation()
        } else {
            findNavController().navigateUp()
        }
    }
}
