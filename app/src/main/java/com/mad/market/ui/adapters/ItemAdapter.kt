package com.mad.market.ui.adapters

import android.annotation.SuppressLint
import android.opengl.Visibility
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textview.MaterialTextView
import com.google.firebase.auth.FirebaseAuth
import com.mad.market.*
import com.mad.market.data.Item
import com.mad.market.ui.items.lists.BoughtItemsListFragment
import com.mad.market.ui.items.lists.ItemListFragment
import com.mad.market.ui.items.lists.ListInterestedFragment
import com.mad.market.ui.items.lists.OnSaleListFragment
import kotlinx.android.synthetic.main.item_layout.view.*
import java.lang.NullPointerException

class ItemAdapter(private val itemList: List<Item>, private val fragment: Fragment) : RecyclerView.Adapter<ItemAdapter.ItemViewHolder>() {
    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val itemName: MaterialTextView = itemView.item_name
        val itemPrice: MaterialTextView = itemView.item_price
        val editButton: ImageButton = itemView.edit_button
        val container: CardView = itemView.container
        val productImage: ImageView = itemView.item_image
        val expired: TextView = itemView.expired_tv
        val rateButton: ImageButton = itemView.rate_button
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false)
        return ItemViewHolder(itemView)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val currentItem = itemList[position]

        if(fragment is BoughtItemsListFragment) {
            if(!currentItem.rated) {
                holder.rateButton.visibility = View.VISIBLE
                holder.rateButton.setOnClickListener {
                    val bundle = bundleOf("itemID" to currentItem.id)
                    bundle.putString("authorID", currentItem.authorID)
                    fragment.findNavController().navigate(R.id.action_boughtItemsListFragment_to_ratingUserFragment, bundle)
                }
            }
            else {
                holder.rateButton.visibility = View.GONE
            }
        }

        holder.itemName.text = currentItem.name
        holder.itemPrice.text = "%.2f".format(currentItem.price) + " €"
        downloadImageFromFirebase("/items/${currentItem.image}", currentItem.image) {
            try { // if we switch view too fast
                holder.productImage.setImageBitmap(rotateImage(it?.let { it1 -> getRotatedBitmap(it1) }, currentItem.imageAngle))
            } catch (e: NullPointerException) {}
        }

        holder.editButton.setOnClickListener {
            val bundle = bundleOf("itemID" to currentItem.id)
            bundle.putString("authorID", currentItem.authorID)
            when (fragment) {
                is ItemListFragment -> fragment.findNavController().navigate(R.id.action_itemListFragment_to_itemEditFragment, bundle)
            }
        }


        holder.container.setOnClickListener {
            val bundle = bundleOf("itemID" to currentItem.id)
            bundle.putString("authorID",currentItem.authorID)
            when (fragment) {
                is ItemListFragment -> fragment.findNavController().navigate(R.id.action_itemListFragment_to_itemDetailsFragment, bundle)
                is OnSaleListFragment -> fragment.findNavController().navigate(R.id.action_onSaleListFragment_to_itemDetailsFragment, bundle)
                is ListInterestedFragment -> fragment.findNavController().navigate(R.id.action_listInterestedFragment_to_itemDetailsFragment, bundle)
                is BoughtItemsListFragment -> fragment.findNavController().navigate(R.id.action_boughtItemsListFragment_to_itemDetailsFragment, bundle)
            }
        }
        if(currentItem.expired()) {
            holder.expired.visibility = View.VISIBLE
        }
        if(currentItem.authorID != getCurrentUserID()
            || currentItem.state == Item.STATE_SOLD) { // cannot edit an already sold item
            holder.editButton.visibility = View.GONE
        }

        when(currentItem.state) {
            Item.STATE_SOLD -> {
                holder.expired.visibility = View.VISIBLE
                holder.expired.setText(R.string.sold)
            }
            Item.STATE_BLOCKED -> {
                holder.expired.visibility = View.VISIBLE
                holder.expired.setText(R.string.blocked)
            }
        }

    }

    override fun getItemCount(): Int {
        return itemList.size
    }

}