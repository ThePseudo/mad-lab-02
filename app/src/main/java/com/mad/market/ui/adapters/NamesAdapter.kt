package com.mad.market.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.mad.market.R
import com.mad.market.data.Item
import com.mad.market.data.User
import com.mad.market.database
import com.mad.market.sendNotification
import kotlinx.android.synthetic.main.name_layout.view.*

class NamesAdapter(private val userList: MutableList<User>,
                   private val item: Item,
                   private val fragment: Fragment)
    : RecyclerView.Adapter<NamesAdapter.NameViewHolder>() {

    class NameViewHolder(personView: View) : RecyclerView.ViewHolder(personView) {
        val container: CardView = personView.container
        val nameView: TextView = personView.name_view
        val sellButton: MaterialButton = personView.sell_btn
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NameViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.name_layout, parent, false)
        return NamesAdapter.NameViewHolder(itemView)
    }

    override fun getItemCount() = run { userList.size }

    override fun onBindViewHolder(holder: NameViewHolder, position: Int) {
        val currentUser = userList[position]
        holder.nameView.text = currentUser.nickName
        if(item.state != Item.STATE_ON_SALE) {
            holder.sellButton.visibility = View.GONE
        }
        else {
            holder.sellButton.visibility = View.VISIBLE
            holder.sellButton.setOnClickListener {
                currentUser.purchasedItems.add(item.id)
                currentUser.save(null) {
                    item.state = Item.STATE_SOLD
                    database.collection("items").document(item.id).set(item)
                        .addOnCompleteListener {
                            for (user in userList) {
                                var title: String = ""
                                var message: String = ""
                                if(user.id != currentUser.id) {
                                    title = fragment.getString(R.string.item_interested_sold)
                                    message = "${item.name} ${fragment.getString(R.string.has_been_sold)}"
                                }
                                else {
                                    title = fragment.getString(R.string.item_purchased)
                                    message = "${fragment.getString(R.string.you_purchased)} ${item.name}"
                                }
                                sendNotification(user.token, title, message)
                                fragment.findNavController().navigateUp()
                            }
                    }
                }
            }
        }
    }

}