package com.mad.market.ui.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.ktx.toObject
import com.mad.market.R
import com.mad.market.data.Item
import com.mad.market.data.Rating
import com.mad.market.data.User
import com.mad.market.database
import kotlinx.android.synthetic.main.rating_layout.view.*

class RatingAdapter(
    private val ratingsList: MutableList<Rating>
): RecyclerView.Adapter<RatingAdapter.RatingViewHolder>() {

    class RatingViewHolder(ratingView: View) : RecyclerView.ViewHolder(ratingView) {
        val authorTV: TextView = ratingView.author_tv
        val itemTV: TextView = ratingView.item_tv
        val itemRating: RatingBar = ratingView.rating_bar
        val feedbackTV: TextView = ratingView.feedback_tv
    }

    override fun getItemCount(): Int {
        return ratingsList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RatingViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.rating_layout, parent, false)
        return RatingViewHolder(itemView)
    }


    override fun onBindViewHolder(holder: RatingViewHolder, position: Int) {
        val currentRating = ratingsList[position]

        holder.itemRating.rating = currentRating.rate
        holder.feedbackTV.text = currentRating.feedback

        database.collection("users").document(currentRating.from).addSnapshotListener { snapshot, exception ->
            if(exception != null) {
                Log.e("User retrivial error", "This should not have happened for user ${currentRating.from}. Ask Andrea about it.")
                return@addSnapshotListener
            }
            if(snapshot != null && snapshot.exists()) {
                val user = snapshot.toObject<User>()
                if(user != null) {
                    holder.authorTV.text = user.nickName
                }
            }
        }

        database.collection("items").document(currentRating.forItem).addSnapshotListener { snapshot, exception ->
            if(exception != null) {
                Log.e("Item retrivial error", "This should not have happened for item ${currentRating.forItem}. Ask Andrea about it.")
                return@addSnapshotListener
            }
            if(snapshot != null && snapshot.exists()) {
                val item = snapshot.toObject<Item>()
                if(item != null) {
                    holder.itemTV.text = item.name
                }
            }
        }
    }
}