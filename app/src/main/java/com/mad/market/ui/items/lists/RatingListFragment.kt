package com.mad.market.ui.items.lists

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.core.app.ActivityCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuth
import com.mad.market.*
import com.mad.market.R
import com.mad.market.data.Filters
import com.mad.market.data.Item
import com.mad.market.data.Rating
import com.mad.market.ui.adapters.ItemAdapter
import com.mad.market.ui.adapters.RatingAdapter
import com.mad.market.ui.profile.ProfileViewModel
import kotlinx.android.synthetic.main.fragment_item_list.*
import kotlinx.android.synthetic.main.fragment_item_on_sale.*
import kotlinx.android.synthetic.main.fragment_item_on_sale.recycler_view
import java.util.*

class RatingListFragment : Fragment() {

    private lateinit var userID: String
    lateinit var viewModel: ProfileViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        userID = arguments?.getString("userID").orEmpty()
        return inflater.inflate(R.layout.fragment_rating_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val fragment = this
        viewModel = ViewModelProvider(this, object: ViewModelProvider.AndroidViewModelFactory(requireActivity().application) {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return ProfileViewModel(userID, activity = requireActivity() as MainActivity) as T
            }
        }).get(ProfileViewModel::class.java)
        viewModel.run {
            user.observe(viewLifecycleOwner) { curUser ->
                val feedbackList = curUser.ratingList
                recycler_view.adapter = RatingAdapter(feedbackList)
                recycler_view.layoutManager = LinearLayoutManager(fragment.requireActivity())
            }
        }
    }
}