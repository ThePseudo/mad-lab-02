package com.mad.market.ui.maps

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.core.app.ActivityCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.Polyline
import com.google.android.gms.maps.model.PolylineOptions
import com.google.android.libraries.places.api.Places
import com.google.android.material.snackbar.Snackbar
import com.mad.market.DEFAULT_ZOOM
import com.mad.market.LOCATION_PERMISSION_CODE
import com.mad.market.MainActivity
import com.mad.market.R


class MapsFragment : Fragment() {

    private lateinit var mapsViewModel: MapsViewModel
    private var mapContext: String? = null
    private var distanceLine: Polyline? = null
    private var currentLocation: LatLng? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mapContext = arguments?.getString(CONTEXT_ID)
        (requireActivity() as MainActivity).supportActionBar?.title = when(mapContext) {
            CONTEXT_EDIT_AD -> {
                setHasOptionsMenu(true)
                getString(R.string.select_ad_location)
            }
            CONTEXT_EDIT_USER -> {
                setHasOptionsMenu(true)
                getString(R.string.select_your_location)
            }
            CONTEXT_SHOW_DISTANCE -> {
                getString(R.string.item_distance)
            }
            CONTEXT_ONLY_VIEW -> {
                getString(R.string.user_location)
            }
            else -> "TESTING MAP HERE!"
        }

        val currentLocationLongitude = arguments?.getDouble(CURRENT_LOCATION_LONGITUDE)
        val currentLocationLatitude = arguments?.getDouble(CURRENT_LOCATION_LATUTUDE)
        if(currentLocationLatitude!= null && currentLocationLongitude != null) {
            currentLocation = LatLng(currentLocationLatitude, currentLocationLongitude)
        }
        if(currentLocation == null) {
            Log.e("Location null", "If in view mode, this is wrong! If in edit, all right!")
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        inflater.inflate(R.menu.menu_save, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when(item.itemId) {
        R.id.menu_save -> {
            val result = bundleOf(RESULT_LATLNG_ID to mapsViewModel.selectedLatLng)
            result.putBoolean(RESULT_LANG_SHOULD_UPDATE, true)
            (requireActivity() as MainActivity).passDataToExternalFragment(result)
            Snackbar.make(requireActivity().findViewById(android.R.id.content), R.string.position_saved, Snackbar.LENGTH_SHORT).show()
            findNavController().navigateUp()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private val callback = OnMapReadyCallback { googleMap ->
        mapsViewModel.map = googleMap
        if(currentLocation == null) {
            currentLocation = mapsViewModel.selectedLatLng
        }
        if(currentLocation != null) {
            mapsViewModel.selectedLatLng = currentLocation
            mapsViewModel.currentMarker = googleMap.addMarker(MarkerOptions().position(mapsViewModel.selectedLatLng!!))
            if(mapsViewModel.firstTime) {
                mapsViewModel.firstTime = false
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                    mapsViewModel.selectedLatLng,
                    DEFAULT_ZOOM.toFloat()
                ))
            }
        }

        checkPermissions()
        getDeviceLocation()

        googleMap.setOnMapClickListener {
            if(mapContext == null || mapContext == CONTEXT_EDIT_USER || mapContext == CONTEXT_EDIT_AD) {
                mapsViewModel.apply {
                    selectedLatLng = it
                    currentMarker?.remove()
                    currentMarker = map.addMarker(MarkerOptions().position(it))
                }

            }
        }
    }

    private fun checkPermissions() {
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION), LOCATION_PERMISSION_CODE)
        }
        else {
            mapsViewModel.locationPermissionGranted = true
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_maps, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapsViewModel = ViewModelProvider(this).get(MapsViewModel::class.java).apply {
            // Construct a PlacesClient
            if(mapContext == CONTEXT_SHOW_DISTANCE)  {
                shouldShowDistance = true
            }
            Places.initialize(requireContext(), getString(R.string.google_maps_key))
            placesClient = Places.createClient(requireContext())
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(requireContext())
        }
        mapFragment?.getMapAsync(callback)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            mapsViewModel.locationPermissionGranted = true
            getDeviceLocation()
        } else {
            findNavController().navigateUp()
        }
    }

    // I already got permission
    @SuppressLint("MissingPermission")
    private fun getDeviceLocation() {
        val locationListener = object : LocationListener {
            override fun onLocationChanged(location: Location?) {
                mapsViewModel.apply {
                    if(location != null) {
                        lastKnownLocation = location
                        if(firstTime && mapContext != CONTEXT_ONLY_VIEW) {
                            firstTime = false
                            map.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                LatLng(lastKnownLocation!!.latitude, lastKnownLocation!!.longitude),
                                DEFAULT_ZOOM.toFloat()
                            ))
                        }
                        if(currentMarker == null) {
                            selectedLatLng = LatLng(lastKnownLocation!!.latitude, lastKnownLocation!!.longitude)
                            currentMarker = map.addMarker(MarkerOptions().position(selectedLatLng!!))
                        }
                        if(shouldShowDistance) {
                            distanceLine?.remove()
                            // Directions API have a price, I don't think it's worth for a university project
                            // If I had those API, I'd add a suspend function, get the JSON response and then draw route as polyline
                            distanceLine = map.addPolyline(PolylineOptions()
                                .add(LatLng(lastKnownLocation!!.latitude, lastKnownLocation!!.longitude))
                                .add(selectedLatLng)
                            )
                        }
                    }
                    else {
                        Log.e("Null location!", "Current location is null. Doing nothing.")
                        map.uiSettings?.isMyLocationButtonEnabled = false
                    }
                }
            }

            override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
            }

            override fun onProviderEnabled(provider: String?) {
            }

            override fun onProviderDisabled(provider: String?) {
            }

        }
        val locationManager = requireContext().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000L, 10.0f, locationListener)

        mapsViewModel.map.isMyLocationEnabled = true
        mapsViewModel.map.uiSettings.isMyLocationButtonEnabled = true

    }

    companion object {
        const val CONTEXT_EDIT_AD = "edit ad"
        const val CONTEXT_EDIT_USER = "edit user"
        const val CONTEXT_SHOW_DISTANCE = "show distance"
        const val CONTEXT_ONLY_VIEW = "only view"
        const val CONTEXT_ID = "contextID"
        const val RESULT_LATLNG_ID = "latlng"
        const val RESULT_LANG_SHOULD_UPDATE = "result lang should update"
        const val CURRENT_LOCATION_LONGITUDE = "marker longitude"
        const val CURRENT_LOCATION_LATUTUDE = "marker latitude"
    }
}