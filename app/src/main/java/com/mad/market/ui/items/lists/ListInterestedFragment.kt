package com.mad.market.ui.items.lists

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager

import com.mad.market.R
import com.mad.market.data.Item
import com.mad.market.ui.adapters.ItemAdapter
import com.mad.market.ui.profile.ProfileViewModel
import kotlinx.android.synthetic.main.fragment_list_interested.*


class ListInterestedFragment : Fragment() {

    lateinit var viewModel: ProfileViewModel
    lateinit var listViewModel: ItemsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_interested, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val fragment = this
        viewModel = ViewModelProvider(requireActivity()).get(ProfileViewModel::class.java).apply {
            user.observe(viewLifecycleOwner, Observer {
                recycler_view.layoutManager = LinearLayoutManager(fragment.requireActivity())

                if(user.value!!.interestedItems.isEmpty()) {
                    image_no_ads.visibility = View.VISIBLE
                    recycler_view.visibility = View.GONE
                    no_ads_on.visibility = View.VISIBLE
                }
                else {
                    image_no_ads.visibility = View.GONE
                    recycler_view.visibility = View.VISIBLE
                    no_ads_on.visibility = View.GONE
                }
            })

            listViewModel = ViewModelProvider(requireActivity()).get(ItemsViewModel::class.java).apply {
                items.observe(viewLifecycleOwner, Observer {
                    val tempItems = mutableListOf<Item>()
                    for(item in items.value!!) {
                        if(item.id in user.value!!.interestedItems) {
                            tempItems.add(item)
                        }
                    }
                    finalItems.value = tempItems
                })

                finalItems.observe(viewLifecycleOwner, Observer {
                    if(listViewModel.finalItems.value!!.isEmpty()){
                        recycler_view.visibility = View.GONE
                        no_ads_on.visibility = View.VISIBLE
                        image_no_ads.visibility = View.VISIBLE

                    }
                    else {
                        // bind list to data
                        recycler_view.visibility = View.VISIBLE
                        no_ads_on.visibility = View.GONE
                        image_no_ads.visibility = View.GONE
                    }
                    recycler_view.adapter = ItemAdapter(listViewModel.finalItems.value!!, fragment)
                })
            }
        }
    }


}
