package com.mad.market.ui.items.lists

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mad.market.data.Filters

class ListFiltersViewModel: ViewModel() {

    var filters = MutableLiveData(Filters())

    fun updateData(filter: Filters) {
        filters.value = filter
    }
}