package com.mad.market.ui.profile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.ktx.toObject
import com.mad.market.data.User
import com.mad.market.*

class EditProfileViewModel(private val userID: String,
                           userEmail: String? = null) : ViewModel() {

    var localPicture: MutableLiveData<String?> = MutableLiveData(null)
    var user: MutableLiveData<User> = MutableLiveData(User())
    var hasLocalPicture = false
    private var alreadyDownloaded = false


    init {
        database.collection("users").document(userID).get().addOnSuccessListener {
            user.value = it.toObject<User>()
            if(user.value == null) {
                user.value = User(email = userEmail.orEmpty(), id = userID)
            }
        }
    }

    fun getImage(forceDownload: Boolean = false, thenRun: (image: String?) -> Unit = {}) {
        if(!hasLocalPicture) {
            if(user.value!!.hasPropic) {
                alreadyDownloaded = true
                downloadImageFromFirebase("user/$userID", userID, forceDownload) {
                    localPicture.value = it.orEmpty()
                    thenRun(it)
                }
            }
            else {
                thenRun(null);
            }
        }
        else {
            thenRun(localPicture.value)
        }
    }

    fun save(thenRun: (success: Boolean) -> Unit = {}) {
        user.value!!.save(localPicture.value, thenRun)
    }
}
