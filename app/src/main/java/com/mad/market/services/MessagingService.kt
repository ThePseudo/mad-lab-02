package com.mad.market.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.mad.market.MainActivity
import com.mad.market.R
import com.mad.market.database
import com.mad.market.getCurrentUserID
import java.lang.Exception

class MessagingService: FirebaseMessagingService() {

    private var notificationID = 0
    private val maxNotificationID = 10

    override fun onMessageReceived(p0: RemoteMessage) {
        if(p0.data.isNotEmpty()) {
            showNotification(p0.data["title"], p0.data["message"])
        }
        if(p0.notification != null) {
            showNotification(p0.notification!!.title, p0.notification!!.body)
        }
    }

    private fun showNotification(title: String?, message: String?) {
        val channelID = "Items channel"
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        val builder = NotificationCompat.Builder(applicationContext, channelID).apply {
            setAutoCancel(true)
            setOnlyAlertOnce(true)
            setContentIntent(pendingIntent)
            setContentTitle(title)
            setContentText(message)
            setSmallIcon(R.drawable.private_user_small)
        }
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel(channelID, channelID, NotificationManager.IMPORTANCE_DEFAULT).also {
                notificationManager.createNotificationChannel(it)
            }
        }
        notificationManager.notify(notificationID, builder.build())
        ++notificationID
        notificationID %= maxNotificationID
    }

    override fun onMessageSent(p0: String) {
        super.onMessageSent(p0)
    }

    override fun onDeletedMessages() {
        super.onDeletedMessages()
    }

    override fun onSendError(p0: String, p1: Exception) {
        super.onSendError(p0, p1)
    }

    override fun onNewToken(token: String) {
        Log.d("Messaging token", token)
        val currentUser = getCurrentUserID()
        if(currentUser != null) {
            database.collection("users").document(currentUser).update("token", token)
        }
    }
}