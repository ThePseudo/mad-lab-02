package com.mad.market

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.os.bundleOf
import androidx.exifinterface.media.ExifInterface
import androidx.lifecycle.ViewModel
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.datepicker.CalendarConstraints
import kotlinx.android.parcel.Parcelize
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileInputStream
import kotlin.math.atan2
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt


const val OPERATION_CAPTURE_PHOTO = 1
const val OPERATION_CHOOSE_PHOTO = 2
const val WRITE_EXTERNAL_STORAGE_PERMISSION_CODE = 1
const val LOCATION_PERMISSION_CODE = 2
const val DEFAULT_ZOOM = 15

const val IMAGE_NAME = "propic.jpg"

fun getRotatedBitmap(path: String): Bitmap? {
    ExifInterface(path).also {
        val orientation = it.getAttributeInt(
            ExifInterface.TAG_ORIENTATION,
            ExifInterface.ORIENTATION_UNDEFINED
        )
        val bitmap = BitmapFactory.decodeFile(path)
        return when(orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(bitmap, 90.0f)
            ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(bitmap, 180.0f)
            ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(bitmap, 270.0f)
            else -> bitmap
        }
    }
}

fun rotateImage(bitmap: Bitmap?, angle: Float): Bitmap? {
    Matrix().also {
        it.postRotate(angle)
        return when(bitmap) {
            null -> null
            else -> Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, it, true)
        }
    }
}

fun saveImage(path: String, context: Context, dest: String? = null): String {
    lateinit var resultPath: String
    var destination = dest.orEmpty()
    if (dest == null)
    {
        resultPath = context.filesDir.absolutePath + File.separator + IMAGE_NAME
        destination = IMAGE_NAME
    }
    else {
        resultPath = context.filesDir.absolutePath + File.separator + destination
    }
    if(path == "") return path
    if((path == resultPath)) return resultPath
    val inputStream = FileInputStream(File(path))
    context.openFileOutput(destination, Context.MODE_PRIVATE).also {
        val bytes = inputStream.readBytes()
        it.write(bytes)
        inputStream.close()
        it.close()
    }
    return resultPath
}

fun saveImage(bitmap: Bitmap, context: Context, dest: String): String {
    context.openFileOutput(dest, Context.MODE_PRIVATE).also {
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
        val byteArray: ByteArray = stream.toByteArray()
        it.write(byteArray)
        it.close()
    }
    return context.filesDir.absolutePath + File.separator + dest
}

fun scaleBitmap(path: String, width: Int, height: Int, context: Context): String {
    val savePath = "temp.jpg"
    if(savePath == path) {
        context.filesDir.absolutePath + File.separator + savePath
    }
    BitmapFactory.decodeFile(path).also {
        val scaleWidth = width.toFloat() / it.width
        val scaleHeight = height.toFloat() / it.height
        val scale = when {
            scaleHeight <= scaleWidth -> scaleWidth
            else -> scaleHeight
        }
        val matrix = Matrix()
        matrix.postScale(scale, scale)
        val scaledBitmap = Bitmap.createBitmap(it, 0, 0, it.width, it.height, matrix, true)
        return saveImage(scaledBitmap, context, savePath)
    }
}

fun hideKeyboardFrom(context: Context, view: View) {
    val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

fun distanceInKm(l1: LatLng, l2: LatLng): Double {
    val R = 6371; // Radius of the earth in km
    val dLat = deg2rad(l2.latitude - l1.latitude)
    val dLon = deg2rad(l2.longitude - l1.longitude)
    val a =
        sin(dLat/2) * sin(dLat/2) +
                cos(deg2rad(l1.latitude)) * cos(deg2rad(l2.latitude)) *
                sin(dLon/2) * sin(dLon/2)
    ;
    val c = 2 * atan2(sqrt(a), sqrt(1-a));
    return R * c; // Distance in km
}

fun deg2rad(deg: Double): Double {
    return deg * (Math.PI/180)
}

@Parcelize
class FutureDateValidator : CalendarConstraints.DateValidator {
    override fun isValid(date: Long): Boolean {
        return date > System.currentTimeMillis()
    }
}

@Parcelize
class PastDateValidator : CalendarConstraints.DateValidator {
    override fun isValid(date: Long): Boolean {
        return date < System.currentTimeMillis()
    }
}

class PassDataViewModel: ViewModel() {
    var data: Bundle = bundleOf()
}