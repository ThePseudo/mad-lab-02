package com.mad.market.data

import android.util.Log
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuth
import com.mad.market.database
import com.mad.market.storage
import java.io.File
import java.io.FileInputStream

data class User(
    var id: String = "",
    var hasPropic: Boolean = false,
    var propicAngle: Float = 0.0f,
    var fullName: String = "",
    var nickName: String = "",
    var email: String = "",
    var location: String = "",
    var latitude: Double? = null,
    var longitude: Double? = null,
    var birthday: Long? = null,
    var phoneNumber: String = "",
    var userType: String = "",
    var nextItemID: Long = 0, // not a real counter, it should ensure unicity of item. Part of Item.id
    var token: String = "",
    var interestedItems: MutableList<String> = mutableListOf(),
    var purchasedItems: MutableList<String> = mutableListOf(),
    var ratingList: MutableList<Rating> = mutableListOf()
) {
    @JvmOverloads
    fun save(localPicture: String?, thenRun: (success: Boolean) -> Unit = {}) {
        //if(userID != FirebaseAuth.getInstance().currentUser!!.uid) return
        val firebaseImagePath = storage.child("users")
            .child("$id.jpg")
        if (localPicture != null) {
            hasPropic = true
            Log.d("Storage", "Saving data online")
            database.collection("users").document(id).set(this).addOnSuccessListener {
                val fis = FileInputStream(File(localPicture))
                firebaseImagePath.putStream(fis).addOnSuccessListener {
                    // Update the profile picture
                    thenRun(true)
                }.addOnCanceledListener {
                    thenRun(false)
                }
            }
        }
        else {
            database.collection("users").document(id).set(this).addOnSuccessListener {
                thenRun(true)
            }
        }
    }
}