package com.mad.market.data

data class Rating(
    var feedback: String = "",
    var rate: Float = 0.0f,
    var from: String = "", // User ID: the user who rated the item
    var forItem: String = "" // Item ID: the item rated
) {

}