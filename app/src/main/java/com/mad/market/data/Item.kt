package com.mad.market.data

import java.util.*



data class Item(
    var authorID: String = "",
    var id: String = "",
    var image: String = "",
    var imageAngle: Float = 0.0f,
    var location: String = "",
    var name: String = "",
    var category: String = "",
    var subcategory: String = "",
    var price: Float? = null,
    var expirationDate: Long? = null,
    var description: String = "",
    var latitude: Double? = null,
    var longitude: Double? = null,
    var interestedPeopleIDs: MutableList<String> = mutableListOf(),
    var rated: Boolean = false,
    var state: String = STATE_ON_SALE
) {

    companion object {
        const val STATE_ON_SALE: String = "on sale"
        const val STATE_SOLD: String = "sold"
        const val STATE_BLOCKED: String = "blocked"
    }

    fun expired() = expirationDate!! < Date().time
}