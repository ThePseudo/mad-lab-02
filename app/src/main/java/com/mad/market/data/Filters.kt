package com.mad.market.data


data class Filters (
    var minPrice: Int = 0,
    var maxPrice: Int = Int.MAX_VALUE,
    var category: String = "",
    var subCategory: String = "",
    var location: String = "", // will be used if we pay Maps API, for premium users
    var distance: Double = 1000.0 // max value
    )
