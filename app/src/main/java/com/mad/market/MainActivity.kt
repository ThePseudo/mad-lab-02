package com.mad.market

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.*
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.navigation.NavigationView
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import com.mad.market.data.User
import com.mad.market.ui.profile.ProfileViewModel
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.nav_header_main.view.*

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var navView: NavigationView
    private lateinit var navController: NavController
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var passDataViewModel: PassDataViewModel
    lateinit var profileViewModel: ProfileViewModel
    private var userRegistered: Boolean? = null // Not yet initialized
    private var firebaseUser: FirebaseUser? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        FirebaseApp.initializeApp(this)
        drawerLayout = findViewById(R.id.drawer_layout)
        navView = findViewById(R.id.nav_view)
        navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(setOf(
            R.id.onSaleListFragment
            ),
            drawerLayout)
        // Drawer shall stay always closed
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        // Load user info
        firebaseUser = login(this)
        val activity = this
        passDataViewModel = ViewModelProvider(this).get(PassDataViewModel::class.java)
        profileViewModel = ViewModelProvider(this, object: ViewModelProvider.NewInstanceFactory() {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return ProfileViewModel(activity = activity) as T
            }

        }).get(ProfileViewModel::class.java).apply {
            // User should be registered?

            user.observe(activity, Observer {user ->
                if(user != null)  {
                    val userID = firebaseUser?.uid
                    if(user.nickName != "") userRegistered = true // this means that the registration has not been completed yet, since it is a mandatory field
                    navView.getHeaderView(0).user_name.text = user.fullName
                    navView.getHeaderView(0).user_nickname.text = user.nickName
                    if(user.hasPropic)
                    {
                        refreshNavView()
                    }
                    if(userID != null) {
                        FirebaseInstanceId.getInstance().instanceId
                            .addOnCompleteListener(OnCompleteListener { task ->
                                if (!task.isSuccessful) {
                                    return@OnCompleteListener
                                }
                                val token = task.result?.token
                                database.collection("users").document(userID).update("token", token)

                                //sendTestNotification()
                            })
                    }
                }
            })
        }

        setupDrawerContent()
    }

    private fun sendTestNotification() {
        FirebaseMessaging.getInstance().apply {
            val data = hashMapOf(
                "title" to "Hello world",
                "message" to "Hey, it's the app!",
                "to" to profileViewModel.user.value!!.token
            )
            FirebaseFunctions.getInstance().getHttpsCallable("sendNotification").call(data)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return item.onNavDestinationSelected(navController) || super.onOptionsItemSelected(item)
    }

    private fun restoreCheckNavView() {
        navView.menu.getItem(0).isChecked = true;
    }

    override fun onNavigateUp(): Boolean {
        restoreCheckNavView()
        hideKeyboardFrom(this, navView)
        if(profileViewModel.user.value!!.fullName == "") {
            navController.popBackStack()
            finish()
        }
        return super.onNavigateUp()
    }

    override fun onBackPressed() {
        if(profileViewModel.shouldRegisterOrResetTrigger.value!!) {
            onNavigateUp()
            return;
        }
        super.onBackPressed()
    }

    private fun setupDrawerContent() {
        navView.menu.getItem(0).isChecked = true;
        navView.setNavigationItemSelectedListener{
            if(!it.isChecked)
            {
                when(it.itemId)
                {

                    R.id.showProfileFragment -> navController.navigate(R.id.action_onSaleListFragment_to_showProfileFragment)
                    R.id.showItemList -> navController.navigate(R.id.action_onSaleListFragment_to_itemListFragment)
                    R.id.interestedListFragment -> navController.navigate(R.id.action_onSaleListFragment_to_listInterestedFragment)
                    R.id.showMarketList -> drawerLayout.closeDrawers()
                    R.id.purchasedItemsFragment -> navController.navigate(R.id.action_onSaleListFragment_to_boughtItemsListFragment)
                }
                drawerLayout.closeDrawers()
            }
            true
        }
    }

    // For last fragment's onPermissionsResult to be called
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        nav_host_fragment.childFragmentManager.fragments[nav_host_fragment.childFragmentManager.fragments.size - 1]
            .onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        restoreCheckNavView()
        if(userRegistered != null) {
            if(!userRegistered!!) {
                Log.e("UNREGISTERED", "User unregistered. This should not happen in the drawer!")
                finish()
            }
        }

        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode) {
            RC_SIGN_IN -> {
                if (resultCode == Activity.RESULT_OK) {
                    firebaseUser = login(this)
                    profileViewModel.reset {
                        profileViewModel.user.value!!.email = firebaseUser!!.email.toString()
                    }
                }
                else {
                    Log.e("REGISTRATION ERROR","Uhm, looks like your registration didn't go well O.o")
                    finish()
                }
            }
        }
    }

    // for extarnal usage
    fun getCurrentUser(): User = run { profileViewModel.user.value!! }

    // Used to pass data back from fragments in a general way
    fun passDataToExternalFragment(b: Bundle) { passDataViewModel.data.putAll(b) }
    fun getDataFromExternalFragment(key: String): Any? {
        return passDataViewModel.data[key]
    }

    // Used when profile is modified, in order not to download the image again
    fun refreshNavView() {
        if(profileViewModel.userID != null) {
            downloadImageFromFirebase("users/${profileViewModel.userID}.jpg", profileViewModel.userID!!) {
                navView.getHeaderView(0).user_propic
                    .setImageBitmap(rotateImage(it?.let { it1 -> getRotatedBitmap(it1) }, profileViewModel.user.value!!.propicAngle))
            }
        }
    }
}
