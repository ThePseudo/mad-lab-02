package com.mad.market

import android.app.Activity
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.storage.ktx.storage
import java.io.File

const val RC_SIGN_IN = 0

val cachedImages = mutableMapOf<String, String?>()

fun login(activity: Activity): FirebaseUser? {
    val firebaseUser: FirebaseUser? = FirebaseAuth.getInstance().currentUser
    if(firebaseUser == null)
    {
        val providers = arrayListOf(
            AuthUI.IdpConfig.GoogleBuilder().build()
        )

        activity.startActivityForResult(
            AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setIsSmartLockEnabled(false)
                .setAvailableProviders(providers)
                .build(), RC_SIGN_IN
        )
    }
    return firebaseUser
}

fun downloadImageFromFirebase(source: String, dest: String, force: Boolean = false, thenRun: (image: String?) -> Unit = {}) {
    var result = cachedImages[dest]
    if((result == null).or(force))
    {
        val tempFile = File.createTempFile(dest, ".jpg")
        storage.child(source)
            .getFile(tempFile).addOnSuccessListener {
                cachedImages[dest] =  tempFile.path
                result = cachedImages[dest]
                thenRun(result)
            }
    }
    else{
        thenRun(result)
    }
}

fun sendNotification(destToken: String, title: String, message: String) {
    if(destToken.isEmpty()) return
    FirebaseMessaging.getInstance().apply {
        val data = hashMapOf(
            "title" to title,
            "message" to message,
            "to" to destToken
        )
        FirebaseFunctions.getInstance().getHttpsCallable("sendNotification").call(data)
    }
}

fun getCurrentUserID(): String? {
    return FirebaseAuth.getInstance().currentUser?.uid
}

val storage = Firebase.storage.reference
val database = Firebase.firestore